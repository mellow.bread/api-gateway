FROM express-gateway

WORKDIR /usr/src/app
COPY package*.json ./
RUN yarn global add express-gateway-plugin-rewrite
RUN yarn
COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]
